# EverCraft Kata Modelling

This project is meant as an ongoing domain model of [this project](https://github.com/PuttingTheDnDInTDD/EverCraft-Kata) along with corresponding unit tests. It is built in Javascript, my preferred language in this case.



## Setup 

1. Clone repository
```bash
git clone https://gitlab.com/ocrahjoshua/evercraft-kata-modelling.git  cd my-project
```
2. cd into project folder

```bash
 cd evercraft-kata-modelling
```

3. Run `npm install` to install dependencies which are actually just development dependencies in this case.

4. Run `npm test` to run the existing unit tests
## Features So Far

### Character Name
- Ability to set character's name
- Ability to get character's name

### Character Alignment
- Ability to set the right alignments (Good, Evil or Newtral)
- Ability to get the defined alignment
- It does not allow an invalid alignment to be set

### Character Armor Class
- Character gets a default armor class of 10


### Character Hit Points
- Character gets default hit points of 5

### Character Attack Functionality
- Character can attack an opponent
- Character effectively hits opponent when roll value meets or beats the opponent's armorClass
- Character always hits when there's a natural roll value of 20

### Character Damage
This is directly related to the attack functionality.
- Character increases the damage of opponent by 1 by getting a hit
- Character increases the damage of opponent by 2 by getting a hit through a natural roll
- Character kills opponent if resulting hitpoints is less than or equal to 0.