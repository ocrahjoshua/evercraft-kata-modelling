class Character {
    static ALIGNMENTS = ['Good', 'Evil', 'Neutral']
    static NATURAL_ROLL = 20

    constructor() {
        this._armorClass = 10
        this._startingHitPoints = 5
        this._damage = 0
        this._isAlive = true
    }

    get name() {
        return this._name
    }

    set name(name) {
        this._name = name
    }

    get alignment() {
        return this._alignment
    }

    set alignment(alignment) {
        if (!Character.ALIGNMENTS.includes(alignment)) {
            throw `Alignment should be either ${Character.ALIGNMENTS.join(', ')}`
        }
        this._alignment = alignment
    }

    get armorClass() {
        return this._armorClass
    }

    get startingHitPoints() {
        return this._startingHitPoints
    }

    set startingHitPoints(hitPoints) {
        this._startingHitPoints = hitPoints
    }

    get currentHitPoints() {
        return this._startingHitPoints - this._damage
    }

    get damage() {
        return this._damage
    }

    set damage(damage) {
        this._damage = damage
    }

    get isAlive() {
        return this._startingHitPoints - this._damage > 0
    }

    get strength () {
        return this._strength
    }

    set strength (strength) {
        this._strength = strength
    }

    attack(opponent, rollValue) {
        if (rollValue === Character.NATURAL_ROLL) {
            opponent.damage += 2
            return true
        }

        const strengthValue = this._strength?.modifier || 0 

        if (rollValue + strengthValue >= opponent.armorClass) {
            opponent.damage += 1 + strengthValue
            return true
        }

        return false
    }
}

module.exports = Character