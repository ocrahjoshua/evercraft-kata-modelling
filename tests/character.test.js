const { expect } = require('chai')

const Character = require('../character')
const Ability = require('../ability')

describe('Character', () => {
    describe('Name', () => {
        it('should be able to set name', () => {
            const player = new Character()
            player.name = 'Josh'
            expect(player.name).to.equal('Josh')
        })
    })

    describe('Alignment', () => {
        it('should be able to set alignment', () => {
            const player = new Character()
            const alignment = 'Good'
            player.alignment = alignment
            expect(player.alignment).to.eql(alignment)
        })

        it('should throw error if alignment is invalid', () => {
            const player = new Character()
            expect(() => player.alignment = 'Diagonal').to.throw('Alignment should be either Good, Evil, Neutral')
        })
    })

    describe('Armor Class', () => {
        it('should default to 10', () => {
            const player = new Character()
            expect(player.armorClass).to.eql(10)
        })
    })

    describe('Hit Points', () => {
        it('should default to 5', () => {
            const player = new Character()
            expect(player.startingHitPoints).to.eql(5)
        })
    })

    describe('Attack', () => {
        describe('Roll value meets armor class', () => {
            it('should return true if roll value meets opponent armor class', () => {
                const player = new Character()
                const opponent = new Character()
                expect(player.attack(opponent, 10)).to.equal(true)
            })

            it('should increase damage by 1 if rollvalue meets opponent armor class', () => {
                const opponent = new Character()
                const player = new Character()
                player.attack(opponent, 10)

                expect(opponent.damage).to.equal(1)
            })

            it('should reduce hitpoints by magnitude of damage', () => {
                const opponent = new Character()
                const player = new Character()

                const initialHitPoints = opponent.startingHitPoints

                player.attack(opponent, 10)

                expect(opponent.currentHitPoints).to.equal(initialHitPoints - 1)
            })
        })


        describe('Roll value beats armor class', () => {
            it('should return true if roll value beats opponent armor class', () => {
                const player = new Character()
                const opponent = new Character()
                expect(player.attack(opponent, 11)).to.equal(true)
            })

            it('should increase damage by 1 if rollvalue beats opponent armor class', () => {
                const opponent = new Character()
                const player = new Character()
                player.attack(opponent, 10)

                expect(opponent.damage).to.equal(1)
            })

            it('should reduce hitpoints by magnitude of damage', () => {
                const opponent = new Character()
                const player = new Character()

                const initialHitPoints = opponent.startingHitPoints

                player.attack(opponent, 10)

                expect(opponent.currentHitPoints).to.equal(initialHitPoints - 1)
            })
        })

        describe('Natural roll value beats armor class', () => {
            it('should return true if natural roll value of 20 hits', () => {
                const player = new Character()
                const opponent = new Character()
                expect(player.attack(opponent, 20)).to.equal(true)
            })

            it('should increase damage by 2 if natural roll value of 20 hits', () => {
                const opponent = new Character()
                const player = new Character()
                player.attack(opponent, 20)

                expect(opponent.damage).to.equal(2)
            })

            it('should reduce hitpoints by magnitude of damage', () => {
                const opponent = new Character()
                const player = new Character()

                const initialHitPoints = opponent.startingHitPoints

                player.attack(opponent, 20)

                expect(opponent.currentHitPoints).to.equal(initialHitPoints - 2)
            })
        })

        it('should kill opponent if hitpoints reduce to 0', () => {
            const opponent = new Character()
            const player = new Character()

            opponent.startingHitPoints = 1
            opponent.armorClass = 1
            player.attack(opponent, 10)

            return expect(opponent.isAlive).to.be.false
        })

        it('should kill opponent if hitpoints reduce to less than 0', () => {
            const opponent = new Character()
            const player = new Character()

            opponent.startingHitPoints = 1
            player.attack(opponent, 20)

            return expect(opponent.isAlive).to.be.false
        })

        it('should not kill opponent if hitpoints is greater than 0', () => {
            const opponent = new Character()
            const player = new Character()

            opponent.startingHitPoints = 2
            opponent.armorClass = 4
            player.attack(opponent, 10)

            return expect(opponent.isAlive).to.be.true
        })

        it('should return false if roll value does not meet or beat opponent armor class', () => {
            const player = new Character()
            const opponent = new Character()
            expect(player.attack(opponent, 4)).to.equal(false)
        })
    })

    describe('Ability Modifiers', () => {
        it('should allow attacker with strength to hit opponent easily', () => {
            const player = new Character()
            const opponent = new Character()

            player.strength = new Ability('Strength')
            player.strength.score = 12
            expect(player.attack(opponent, 9)).to.equal(true)
        })

        it('should cause more damage for attacker with strength', () => {
            const player = new Character()
            const opponent = new Character()

            player.strength = new Ability('Strength')
            player.strength.score = 12

            player.attack(opponent, 10)

            expect(opponent.damage).to.equal(2)
        })
    })
})