const { expect } = require('chai')

const Ability = require('../ability')



describe('Ability', () => {
    describe('Type', () => {
        ['Strength', 'Dexterity', 'Constitution', 'Wisdom', 'Intelligence', 'Charisma'].forEach((singleAbility) => {
            it(`should be able to set type ${singleAbility} initially`, () => {
                const ability = new Ability(singleAbility)
                expect(ability.type).to.equal(singleAbility)
            })
        })

        it('should throw error if type is invalid', () => {
            expect(() => new Ability('Resilience')).to.throw('Ability is not valid')
        })
    })


    describe('Score', () => {
        it('should default to 10', () => {
            const ability = new Ability('Strength')
            expect(ability.score).to.equal(10)
        })

        it('should throw error if range is less than 1', () => {
            const ability = new Ability('Strength')
            expect(() => ability.score = 0).to.throw('Score ranges 1 to 20')
        })
    })

    describe('Modifier', () => {
        [{ score: 1, modifier: -5 }, { score: 2, modifier: -4 }, { score: 3, modifier: -4 }].forEach(item => {
            it(`should have a modifier of ${item.modifier} for score of ${item.score}`, () => {
                const ability = new Ability('Strength')
                ability.score = item.score
                expect(ability.modifier).to.equal(item.modifier)
            })
        })
    })
})