
const ABILITIES = ['Strength', 'Dexterity', 'Constitution', 'Wisdom', 'Intelligence', 'Charisma']
class Ability {

    constructor(type) {
        this.type = type
        this._score = 10
    }
    get type() {
        return this._type
    }

    set type(type) {
        if (!ABILITIES.includes(type)) {
            throw new Error('Ability is not valid')
        }
        this._type = type
    }

    get score() {
        return this._score
    }

    set score(score) {
        if (score < 1 || score > 20) {
            throw new Error('Score ranges 1 to 20')
        }
        this._score = score
    }

    get modifier() {
        return Math.floor((this._score - 10) / 2)
    }
}

module.exports = Ability